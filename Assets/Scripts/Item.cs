﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Item
    {

        public struct Damage
        {
            public int min;
            public int max;
        }

        public string name;

        public Item(string name)
        {
            this.name = name;
        }

        public virtual string Print()
        {
            return name;
        }
    }
}
