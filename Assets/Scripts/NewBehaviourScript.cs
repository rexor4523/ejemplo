﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConsoleApp1;

public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        string str;

        Debug.Log("1 - sword");
        Debug.Log("2 - shield");
        str = "1";//Console.ReadLine();

        Item item = null;
        Debug.Log("Name: ");
        String name = "1";//Console.ReadLine();

        switch (int.Parse(str))
        {
            case 1:
                Debug.Log("Min: ");
                string min = "1"; //Console.ReadLine();

                Debug.Log("Max: ");
                string max = "5";//Console.ReadLine();

                Item.Damage dmg = new Item.Damage();
                dmg.min = int.Parse(min);
                dmg.max = int.Parse(max);

                item = new Sword(name, dmg);
                break;

            case 2:
                Debug.Log("Ac: ");
                string ac = "1";//Console.ReadLine();

                item = new Shield(name, int.Parse(ac));
                break;
        }

        if (item != null)
            Debug.Log(item.Print());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
