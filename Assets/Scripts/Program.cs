﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            string str;

            Console.WriteLine("1 - sword");
            Console.WriteLine("2 - shield");
            str = Console.ReadLine();

            Item item = null;
            Console.WriteLine("Name: ");
            String name = Console.ReadLine();

            switch (int.Parse(str))
            {
                case 1:
                    Console.WriteLine("Min: ");
                    string min = Console.ReadLine();

                    Console.WriteLine("Max: ");
                    string max = Console.ReadLine();

                    Item.Damage dmg = new Item.Damage();
                    dmg.min = int.Parse(min);
                    dmg.max = int.Parse(max);

                    item = new Sword(name, dmg);
                    break;

                case 2:
                    Console.WriteLine("Ac: ");
                    string ac = Console.ReadLine();

                    item = new Shield(name, int.Parse(ac));
                    break;
            }

            if (item != null)
                Console.WriteLine(item.Print());

        }
    }
}
