﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Shield : Item
    {
        public int Ac;

        public Shield(string name, int Ac) : base(name)
        {
            this.Ac = Ac;
 
        }

        public override string Print()
        {
            return "Shield.name = " + name +
                "nshield.Ac = " + Ac + "-";
        }

    }
}
