﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Sword : Item
    {

        public Damage damage;

        public Sword(string name, Damage damage) : base(name)
        {
            this.damage = damage;
        }

        public override string Print()
        {
            return "sword.name = " + name +
                "nSword.damage = " + damage.min + "-";
        }

    }
}
